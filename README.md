# Simple Microservice Example

## Requirements:
- Postgres >= 12.4
- Redis
- [Pyenv](https://github.com/pyenv/pyenv) is recommended
## Setup
### Copy config file and fill it with needed keys
```
$ cp local_example.yaml local.yaml
```
