from datetime import datetime
from aiohttp import web
from aiohttp_apispec import docs, response_schema
from api import schema


class WhatTimeIsIt(web.View):
    @docs(
        tags=["Time"],
        summary="Display current time",
        description="Return the dict with the current datetime",
    )
    @response_schema(
        schema=schema.SuccessSchema(),
        code=200,
        description="Return success response with dict",
    )
    @response_schema(
        schema=schema.BadRequestSchema(),
        code=400,
        description="Return success response with dict",
    )
    async def get(self):
        return web.json_response({"result": datetime.now().strftime('%Y-%m-%d-%H:%M:%S')})
