from yarl import URL
from aiohttp import web
from .views import what_time_is_it, how_to_fibo, lets_dict


def setup_routes(app: web.Application) -> None:
    """Setup routes for application."""
    # API
    base = URL("/api/v1")
    app.router.add_view(to_path(base / "what-time-is-it"), what_time_is_it.WhatTimeIsIt)
    app.router.add_view(to_path(base / "how-to-fibo" / "{value}"), how_to_fibo.HowToFibo)
    app.router.add_view(to_path(base / "lets_dict"), lets_dict.LetsDict)


def to_path(url: URL, *, has_trailing_slash: bool = True) -> str:
    """
    Convert URL instance into string path, suitable for aiohttp.web router.

    When `has_trailing_slash` is `True` - append trailing slash for URL, if it not
    already appended.
    """
    # Do not append trailing slash if it already added
    if url.parts[-1]:
        url = url / "" if has_trailing_slash else url
    return url.human_repr()
